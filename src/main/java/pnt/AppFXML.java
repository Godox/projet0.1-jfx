package pnt;

import controleur.ControleurImpl;
import javafx.application.Application;
import javafx.stage.Stage;
import modele.FacadeModele;
import vues.gestion.GestionnairevueImpl;
import vues.interfaces.GestionnaireVue;
import vues.interfaces.Vue;

import java.io.IOException;

public class AppFXML extends Application {
    @Override
    public void start(Stage stage) throws IOException {
        GestionnaireVue gestionnaireVue = new GestionnairevueImpl(stage);
        FacadeModele facadeModele = null; //TODO Ajouter une instance de la facade
        ControleurImpl controle = new ControleurImpl(gestionnaireVue, facadeModele,
                (controleur, gestionnaireVue1) -> {
                    // Propagation du contrôleur pour toutes les vues
                    gestionnaireVue1.getVuesInteractives().forEach(vueInteractive ->
                            vueInteractive.setControleur(controleur));

                    // Inscription des ecouteurs d'ordres
                    gestionnaireVue1.getEcouteurOrdres().forEach(ecouteurOrdre ->
                            ecouteurOrdre.setAbonnement(controleur));

                    // Execution de la méthode afterControlerinitialisation
                    gestionnaireVue1.getVuesInteractives().forEach(Vue::afterControlerInitialisation);
                });

        controle.run();
    }


    public static void main(String[] args) {
        launch();
    }
}