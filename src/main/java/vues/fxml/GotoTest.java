package vues.fxml;

import controleur.ordres.ActionOrdre;
import controleur.ordres.LanceurOrdre;
import vues.abstractVues.AbstractVueInteractiveFxml;

public class GotoTest extends AbstractVueInteractiveFxml {

    @Override
    public void initialisation() {
        super.initialisation();
    }

    @Override
    public void afterControlerInitialisation() {
        super.afterControlerInitialisation();
    }

    @Override
    public String getFXmlFileName() {
        return "gotoTest.fxml";
    }

}