package vues.fxml;

import controleur.ordres.*;
import vues.abstractVues.AbstractVueInteractiveFxml;

public class Home extends AbstractVueInteractiveFxml implements EcouteurAction {

    @Override
    public void initialisation() {
        super.initialisation();
    }

    @Override
    public void afterControlerInitialisation() {
        super.afterControlerInitialisation();
    }

    public String getFXmlFileName() {
        return "home.fxml";
    }

    public void sayHello(){
        this.getControleur().sayHello();
    }

    @Override
    public void traiterActions(ActionOrdre e) {

    }

    @Override
    public void setAbonnement(LanceurOrdre g) {

    }
}

