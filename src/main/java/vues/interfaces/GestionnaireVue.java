package vues.interfaces;

import controleur.ordres.EcouteurOrdre;
import javafx.stage.Stage;

import java.util.Collection;

public interface GestionnaireVue extends EcouteurOrdre {
    Stage getStage();

    void ajouterEcouteurOrdre(EcouteurOrdre ecouteurOrdre);

    Collection<EcouteurOrdre> getEcouteurOrdres();

    void ajouterVueInteractive(VueInteractive vueInteractive);

    Collection<VueInteractive> getVuesInteractives();
}
