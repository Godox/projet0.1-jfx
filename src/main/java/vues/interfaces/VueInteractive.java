package vues.interfaces;

import controleur.ControleurImpl;
import controleur.ordres.EcouteurOrdre;

public interface VueInteractive extends Vue {
    ControleurImpl getControleur();

    void setControleur(ControleurImpl controleur);
}
