package vues.gestion;

import controleur.ordres.ActionOrdre;
import controleur.ordres.GoToOrdre;
import controleur.ordres.LanceurOrdre;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import vues.abstractVues.AbstractGestionnaireVue;
import vues.fxml.GotoTest;
import vues.fxml.Home;


public class GestionnairevueImpl extends AbstractGestionnaireVue {

//    LOAD FXML VIEW EXAMPLE
    private final Home home = this.loadFxmlView(Home.class);
    private final GotoTest test = this.loadFxmlView(GotoTest.class);

    public GestionnairevueImpl(Stage stage) {
        super(stage);
        this.initialize();
        getStage().setScene(this.home.getScene());
    }

    @Override
    public void setAbonnementActions(LanceurOrdre g) {
        g.abonnement(this, ActionOrdre.ORDRE_HELLO);
    }


    @Override
    public void traiterActions(ActionOrdre e) {
            switch (e){
                case ORDRE_HELLO: this.sayHello();
            }
    }

    @Override
    public void setGotos() {

        GoToOrdre.GOTO_HOME.setGotoView(home);
        GoToOrdre.GOTO_TEST.setGotoView(test);

    }

    private void sayHello() {
        Alert al = new Alert(Alert.AlertType.ERROR, "Hello World");
        al.setTitle("Hello");
        al.showAndWait();
    }

}
