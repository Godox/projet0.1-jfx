package vues.abstractVues;

import controleur.ordres.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.stage.Stage;
import vues.interfaces.GestionnaireVue;
import vues.interfaces.Vue;
import vues.interfaces.VueInteractive;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

public abstract class AbstractGestionnaireVue implements GestionnaireVue {
    private Stage stage;

    public abstract void traiterActions(ActionOrdre actionOrdre);

    public abstract void setGotos();

    @Override
    public Stage getStage() {
        return stage;
    }

    private Collection<EcouteurOrdre> ecouteurOrdres;

    private Collection<VueInteractive> vuesInteractives;

    public AbstractGestionnaireVue(Stage stage) {
        this.stage = stage;
        this.ecouteurOrdres = new ArrayList<>();
        this.vuesInteractives = new ArrayList<>();
        this.ecouteurOrdres.add(this);

    }

    public void setDisplayedView(Vue view) {
        getStage().setScene(view.getScene());
        getStage().show();
    }

    @Override
    public void ajouterEcouteurOrdre(EcouteurOrdre ecouteurOrdre) {
        this.ecouteurOrdres.add(ecouteurOrdre);
    }

    @Override
    public Collection<EcouteurOrdre> getEcouteurOrdres() {
        return ecouteurOrdres;
    }

    @Override
    public void ajouterVueInteractive(VueInteractive vueInteractive) {
        this.vuesInteractives.add(vueInteractive);
    }

    public <T extends AbstractVueInteractiveFxml> T loadFxmlView(Class<T> typeClass) {
        String clsName = typeClass.getName();
        String filename;
        try {
            filename = typeClass.getMethod("getFXmlFileName").invoke(typeClass.getConstructor().newInstance()).toString();
            FXMLLoader fxmlLoader = new FXMLLoader(typeClass.getResource(filename));
            try {
                Parent p = fxmlLoader.load();
                T v = fxmlLoader.getController();
                v.setParent(p);
                v.initialisation();
                return v;
            } catch (IOException | IllegalStateException e) {
                System.out.println("WARNING: Unable to load FXML view with name \"" + filename + "\" :" + e.getMessage());
                e.printStackTrace();
            }
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            System.out.println("getFXmlFileName method is not implemented in " + clsName);
        } catch (InstantiationException e) {
            System.out.println("Error when calling constructor of " + clsName);
        }
        return null;
    }

    public void traiterGoto(GoToOrdre e) {
        setDisplayedView(e.getView());
    }

    @Override
    public void traiter(Ordre e) {
        switch (e.getType()) {
            case ACTION -> this.traiterActions((ActionOrdre) e);
            case GOTO -> this.traiterGoto((GoToOrdre) e);
        }
    }

    @Override
    public void setAbonnement(LanceurOrdre g) {
        Arrays.stream(GoToOrdre.values()).forEach((o) -> g.abonnement(this, o));
        setAbonnementActions(g);
    }

    public abstract void setAbonnementActions(LanceurOrdre g);


    public void ajouterAllVuesInteractives() {
        for (Field declaredField : this.getClass().getDeclaredFields()) {
            if (VueInteractive.class.isAssignableFrom(declaredField.getType())) {
                try {
                    declaredField.setAccessible(true);
                    VueInteractive vue = (VueInteractive) declaredField.get(this);
                    if (vue != null) {
                        this.ajouterVueInteractive(vue);
                        System.out.println("Added " + vue.getClass().getSimpleName() + " to VuesInteractives");
                    }
                } catch (ClassCastException | IllegalAccessException e) {
                    System.out.println("Error when adding " + declaredField.getClass().getSimpleName() + " to VuesInteractives :" + e);
                }
            }
        }
    }

    public void ajouterAllEcouteursOrdres() {
        for (Field declaredField : this.getClass().getDeclaredFields()) {
            if (EcouteurOrdre.class.isAssignableFrom(declaredField.getType())) {
                try {
                    declaredField.setAccessible(true);
                    EcouteurOrdre vue = (EcouteurOrdre) declaredField.get(this);
                    if (vue != null) {
                        this.ajouterEcouteurOrdre(vue);
                        System.out.println("Added " + vue.getClass().getSimpleName() + " to EcouteursOrdres");
                    }
                } catch (ClassCastException | IllegalAccessException e) {
                    System.out.println("Error when adding " + declaredField.getClass().getSimpleName() + " to EcouteursOrdres :" + e);
                }
            }
        }
    }

    @Override
    public Collection<VueInteractive> getVuesInteractives() {
        return this.vuesInteractives;
    }

    protected void initialize() {
        this.ajouterAllVuesInteractives();
        this.ajouterAllEcouteursOrdres();
        this.setGotos();
    }
}
