package controleur;

import vues.interfaces.GestionnaireVue;

public interface ControleurSetUp {

    void setUp(ControleurImpl controleur, GestionnaireVue gestionnaireVue);
}
