package controleur.ordres;


import vues.interfaces.Vue;

public enum GoToOrdre implements Ordre{

    GOTO_HOME,
    GOTO_TEST;

    private Vue view;

    @Override
    public Type getType() {
        return Type.GOTO;
    }

    public void setGotoView(Vue vue) {
        this.view = vue;
    }

    public Vue getView() {
        return view;
    }
}
