package controleur.ordres;

public interface Ordre {

    Type getType();

    enum Type {
        // Type des ordres
        GOTO,
        ACTION;

    }

}
