package controleur.ordres;

public enum ActionOrdre implements Ordre{

    ORDRE_HELLO;

    @Override
    public Type getType() {
        return Type.ACTION;
    }
}
