package controleur.ordres;

public interface EcouteurAction extends EcouteurOrdre{

    @Override
    default void traiter(Ordre e) {
        if (e.getType() == Ordre.Type.ACTION) {
            this.traiterActions((ActionOrdre) e);
        }
    }

    void traiterActions(ActionOrdre e);

}
