package controleur;

import controleur.abstractControleur.AbstractControleur;
import controleur.ordres.ActionOrdre;
import modele.FacadeModele;
import vues.interfaces.GestionnaireVue;

import static controleur.ordres.GoToOrdre.GOTO_HOME;


public class ControleurImpl extends AbstractControleur {

    private FacadeModele facadeModele;

    public ControleurImpl(GestionnaireVue gestionnaireVue, FacadeModele facadeModele, ControleurSetUp controleurSetUp) {
        super(gestionnaireVue);
        this.facadeModele = facadeModele;
        controleurSetUp.setUp(this, getGestionnaireVue());
    }

    @Override
    public void run() {
        this.fireOrdre(GOTO_HOME);
    }

    public void sayHello() {
        this.fireOrdre(ActionOrdre.ORDRE_HELLO);
    }
}
